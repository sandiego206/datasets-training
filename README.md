# Entrenamientos con Caffe #

Enlaces a la fuente original para descargar cada dataset.

1. [Flowers17](http://www.robots.ox.ac.uk/~vgg/data/flowers/17/) 
2. [Flowers102](http://www.robots.ox.ac.uk/~vgg/data/flowers/102/)
3. [Caltech101](http://www.vision.caltech.edu/Image_Datasets/Caltech101/)
4. [Scene67](http://web.mit.edu/torralba/www/indoor.html)

A continuación se encuentran los reportes de los entrenamientos sobre cada dataset.

1. [Reporte Flowers17](http://nbviewer.ipython.org/urls/bitbucket.org/sandiego206/datasets-training/raw/8d822cedb322d0d545930b87327da793513e0951/Reporte%20Flowers17.ipynb)
2. [Reporte Flowers102](http://nbviewer.ipython.org/urls/bitbucket.org/sandiego206/datasets-training/raw/8d822cedb322d0d545930b87327da793513e0951/Reporte%20Flowers102.ipynb)
3. [Reporte Caltech101](http://nbviewer.ipython.org/urls/bitbucket.org/sandiego206/datasets-training/raw/8d822cedb322d0d545930b87327da793513e0951/Reporte%20Caltech%20101.ipynb)
4. [Reporte Scene67](http://nbviewer.ipython.org/urls/bitbucket.org/sandiego206/datasets-training/raw/8d822cedb322d0d545930b87327da793513e0951/Reporte%20Scene67.ipynb)